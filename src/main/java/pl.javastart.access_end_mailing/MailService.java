package pl.javastart.access_end_mailing;

/**
 * Klasa do wysyłania maili. Nie zmieniaj zawartości tej klasy!
 */
public class MailService {

    /**
     * Metoda do wysyłania maili. Nie zmieniaj zawartości tej metody!
     */
    public void sendEmail(String emailAddress, String title, String content) {
        throw new RuntimeException("Nie wysyłaj maili z poziomu testów jednostkowych!");
    }
}
