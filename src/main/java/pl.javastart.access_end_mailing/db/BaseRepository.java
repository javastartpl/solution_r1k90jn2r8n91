package pl.javastart.access_end_mailing.db;

import java.util.List;

public abstract class BaseRepository<T> {

    private static final String MESSAGE = "Nie używaj metod pobierających dane z bazy danych w testach jednostkowych!";

    public T findById(Long id) {
        throw new RuntimeException(MESSAGE);
    }

    public List<T> findAll() {
        throw new RuntimeException(MESSAGE);
    }
}
